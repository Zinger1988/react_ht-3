import * as yup from 'yup';

const validationSchema = yup.object().shape({
	email: yup
		.string()
		.matches(/.+@[^@]+\.[^@]{2,}$/, 'Invalid email')
		.required(),
	password: yup.string().min(8).required(),
});

export default validationSchema;
