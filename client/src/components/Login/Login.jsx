import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import { Link, useNavigate } from 'react-router-dom';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import ShortMessage from '../../common/ShortMessage/ShortMessage';

import API from '../../store/services';
import { clearErrorsAction } from '../../store/errors/actionCreators';
import { loginUserAction } from '../../store/user/actionCreators';
import { useDispatch, useSelector } from 'react-redux';
import { errorsSelector } from '../../store/errors/selectors';

import validationSchema from './validationSchema';

import './Login.scss';

const Login = (props) => {
	const { isAuth, className } = props;
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const errors = useSelector(errorsSelector);

	const errorMessage = errors.map((error, i) => {
		const message = error.data.result;
		return <ShortMessage key={i} type='alert' message={message} />;
	});

	useEffect(() => {
		isAuth && navigate('/');
	}, [isAuth]);

	const handleSubmit = (values) => {
		dispatch(clearErrorsAction());
		API.loginService(values).then((res) => {
			dispatch(loginUserAction(res));
			navigate('/courses');
		});
	};

	return (
		<div className={`login ${className}`}>
			<div className='container container--narrow'>
				<h1 className='page-title'>Login</h1>
				<Formik
					initialValues={{
						email: '',
						password: '',
					}}
					validateOnChange={false}
					validateOnBlur={false}
					validationSchema={validationSchema}
					onSubmit={handleSubmit}
				>
					{() => (
						<Form className='login__form form'>
							<div className='form__body'>
								<Input
									className='form__input'
									name='email'
									label='Email'
									placeholder='Enter your email...'
								/>
								<Input
									className='form__input'
									type='password'
									name='password'
									label='Password'
									placeholder='Enter your password...'
								/>
							</div>
							{errorMessage.length > 0 && (
								<div className='form__errors'>{errorMessage}</div>
							)}
							<div className='form__footer'>
								<Button
									type='submit'
									buttonText='Login'
									cssStyle='solid-yellow'
									size='lg'
									fluid
								/>
							</div>
						</Form>
					)}
				</Formik>
				<p>
					If you not have an account you can{' '}
					<Link to='/registration'>register</Link>
				</p>
				<p>
					<b>Admin credentials (demo purposes only)</b>
					<br />
					login: admin@email.com
					<br />
					Password: admin123
				</p>
			</div>
		</div>
	);
};

Login.propTypes = {
	className: PropTypes.string,
};

Login.defaultProps = {
	className: '',
};

export default Login;
