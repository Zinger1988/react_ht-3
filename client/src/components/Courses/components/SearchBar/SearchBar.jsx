import PropTypes from 'prop-types';
import React from 'react';
import { Form, withFormik } from 'formik';

import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';

import './SearchBar.scss';

const SearchBar = (props) => {
	const { className, resetSearch, setFieldValue, values } = props;

	const handleChange = () => !values.search && resetSearch();
	const handleBlur = () => !values.search.trim() && setFieldValue('search', '');

	return (
		<Form className={`search-bar ${className}`}>
			<Input
				className='search-bar__input'
				placeholder='Enter course name or course ID...'
				cssStyle='white-solid'
				name='search'
				onChange={handleChange}
				onBlur={handleBlur}
			/>
			<Button
				type='submit'
				buttonText='Search'
				className='search-bar__search-btn'
				cssStyle='solid-yellow'
			/>
		</Form>
	);
};

SearchBar.propTypes = {
	className: PropTypes.string,
	searchCards: PropTypes.func.isRequired,
	resetSearch: PropTypes.func.isRequired,
};

SearchBar.defaultProps = {
	className: '',
};

const handleSubmit = (values, helpers) => {
	const { search } = values;
	const { searchCards } = helpers.props;
	searchCards(search.trim());
};

export default withFormik({
	mapPropsToValues: (props) => ({
		search: '',
	}),
	handleSubmit,
})(SearchBar);
