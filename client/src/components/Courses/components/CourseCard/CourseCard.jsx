import PropTypes from 'prop-types';
import React from 'react';
import { useNavigate } from 'react-router-dom';

import Button from '../../../../common/Button/Button';
import ShortInfoList from '../../../../common/ShortInfoList/ShortInfoList';

import API from '../../../../store/services';
import { deleteCourseAction } from '../../../../store/courses/actionCreators';
import { useDispatch } from 'react-redux';

import './CourseCard.scss';

const CourseCard = (props) => {
	const { cardData, className } = props;
	const { id, title, duration, creationDate, description, authors } = cardData;
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const shortInfoData = [
		{ title: 'Authors', data: authors },
		{ title: 'Duration', data: duration },
		{ title: 'Created', data: creationDate },
	];

	const handleCourseDelete = () => {
		API.deleteCourseService(id).then(() => {
			dispatch(deleteCourseAction(id));
		});
	};

	return (
		<article className={`course-card ${className}`}>
			<div className='course-card__col'>
				<h2 className='course-card__title'>{title}</h2>
				<p className='course-card__id'>ID: {id}</p>
				<p className='course-card__description'>{description}</p>
			</div>
			<div className='course-card__col'>
				<ShortInfoList className='course-card__info' listData={shortInfoData} />

				<div className='course-card__btn-group'>
					<Button
						buttonText='Show course'
						cssStyle='solid-yellow'
						onClick={() => navigate(id)}
					/>
					<Button buttonText='Delete course' onClick={handleCourseDelete} />
				</div>
			</div>
		</article>
	);
};

CourseCard.propTypes = {
	cardData: PropTypes.exact({
		authors: PropTypes.string,
		creationDate: PropTypes.string,
		description: PropTypes.string,
		duration: PropTypes.string,
		id: PropTypes.string,
		title: PropTypes.string,
	}).isRequired,
	className: PropTypes.string,
};

CourseCard.defaultProps = {
	className: '',
};

export default CourseCard;
