import Button from '../../common/Button/Button';
import React from 'react';
import { useNavigate } from 'react-router-dom';

import './page404.scss';

const Page404 = () => {
	const navigate = useNavigate();

	return (
		<div className='page404 my-auto'>
			<div className='container page404__container'>
				<h1 className='page404__title'>404</h1>
				<p className='page404__description'>This page is not exist</p>
				<div className='page404__btn-group'>
					<Button
						size='lg'
						cssStyle='solid-yellow'
						buttonText='Go to main page'
						onClick={() => navigate('/')}
					/>
				</div>
			</div>
		</div>
	);
};

export default Page404;
