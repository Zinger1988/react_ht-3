import * as yup from 'yup';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';

import AuthorsList from './components/AuthorsList/AuthorsList';
import Button from '../../common/Button/Button';
import CourseDuration from './components/CourseDuration/CourseDuration';
import CreateAuthor from './components/CreateAuthor/CreateAuthor';
import Input from '../../common/Input/Input';
import ShortMessage from '../../common/ShortMessage/ShortMessage';
import Textarea from '../../common/Textarea/Textarea';

import API from '../../store/services';
import Course from '../../helpers/Course';
import { addCourseAction } from '../../store/courses/actionCreators';
import { authorsSelector } from '../../store/authors/selectors';
import { useDispatch, useSelector } from 'react-redux';

import './CreateCourse.scss';

const CreateCourse = (props) => {
	const { getAuthorsById, className } = props;
	const [courseAuthors, setCourseAuthors] = useState([]);
	const [duration, setDuration] = useState('');

	const dispatch = useDispatch();
	const navigate = useNavigate();

	const authors = useSelector(authorsSelector);
	const selectedAuthors = getAuthorsById(authors, courseAuthors);
	const availableAuthors = authors.filter((author) => {
		return !courseAuthors.some((auth) => auth === author.id);
	});

	const validationSchema = yup.object().shape({
		title: yup.string().required('This field is required'),
		description: yup.string().required('This field is required'),
		duration: yup
			.number()
			.typeError('Only numbers allowed')
			.required('This field is required'),
		courseAuthors: yup
			.array()
			.test('course-authors', 'You should add at least one author', () => {
				return courseAuthors.length > 0;
			}),
	});

	const handleSubmit = (values) => {
		const { duration, ...restValues } = values;
		const courseData = new Course({
			duration: +duration,
			authors: courseAuthors,
			...restValues,
		});
		API.addCourseService(courseData).then((res) => {
			dispatch(addCourseAction(res));
			navigate('/courses');
		});
	};

	const handleSelectedAuthors = (author) => {
		setCourseAuthors(courseAuthors.filter((a) => a !== author.id));
	};

	const handleAvailableAuthors = (author) => {
		setCourseAuthors([...courseAuthors, author.id]);
	};

	return (
		<div className={`create-course ${className}`}>
			<div className='container create-course__container'>
				<Formik
					initialValues={{
						title: '',
						description: '',
						duration: '',
						newAuthor: '',
					}}
					validateOnChange={false}
					validateOnBlur={false}
					validationSchema={validationSchema}
					onSubmit={handleSubmit}
				>
					{(formikProps) => (
						<Form className='create-course__form course-form'>
							<fieldset className='course-form__head'>
								<Input
									className='course-form__input course-form__input--title'
									placeholder='Enter title...'
									label='Title'
									name='title'
								/>
								<Button
									cssStyle='solid-yellow'
									buttonText='Create course'
									type='submit'
								/>
								<Button
									onClick={() => navigate('/courses')}
									cssStyle='solid-black'
									buttonText='Cancel'
								/>
								<Textarea
									className='course-form__textarea course-form__textarea--description'
									placeholder='Enter description...'
									label='Description'
									name='description'
								/>
							</fieldset>
							<fieldset className='course-form__body'>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Create author</h3>
									<CreateAuthor />
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Authors</h3>
									<AuthorsList
										className='course-form__author-list'
										list={availableAuthors}
										onClick={handleAvailableAuthors}
										buttonText='Add author'
									/>
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Duration</h3>
									<CourseDuration
										setDuration={setDuration}
										duration={duration}
									/>
								</div>
								<div className='course-form__group'>
									<h3 className='course-form__group-title'>Course authors</h3>
									{formikProps.errors.courseAuthors && (
										<ShortMessage
											className='course-form__message'
											type='alert'
											message={formikProps.errors.courseAuthors}
										/>
									)}
									<AuthorsList
										className='course-form__author-list'
										list={selectedAuthors}
										onClick={handleSelectedAuthors}
										buttonText='Remove author'
									/>
								</div>
							</fieldset>
						</Form>
					)}
				</Formik>
			</div>
		</div>
	);
};

CreateCourse.propTypes = {
	getAuthorsById: PropTypes.func.isRequired,
	className: PropTypes.string,
};

CreateCourse.defaultProps = {
	className: '',
};

export default CreateCourse;
