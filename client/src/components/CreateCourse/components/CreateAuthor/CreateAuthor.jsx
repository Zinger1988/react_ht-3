import React from 'react';
import { useDispatch } from 'react-redux';
import { useFormikContext } from 'formik';

import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';

import API from '../../../../store/services';
import { addAuthorAction } from '../../../../store/authors/actionCreators';

import Author from '../../../../helpers/Author';

const CreateAuthor = () => {
	const dispatch = useDispatch();
	const formikContext = useFormikContext();

	const createAuthor = () => {
		const { setFieldValue } = formikContext;
		const { newAuthor } = formikContext.values;

		if (newAuthor.trim()) {
			const authorData = new Author(newAuthor.trim());
			API.addAuthorService(authorData).then((res) => {
				dispatch(addAuthorAction(res));
			});
			setFieldValue('newAuthor', '');
		}
	};

	return (
		<>
			<Input
				className='course-form__input'
				placeholder='Enter author name...'
				label='Author name'
				name='newAuthor'
			/>
			<Button onClick={() => createAuthor()} buttonText='Create author' />
		</>
	);
};

export default CreateAuthor;
