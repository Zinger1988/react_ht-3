import React, { useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import CourseInfo from '../CourseInfo/CourseInfo';
import Courses from '../Courses/Courses';
import CreateCourse from '../CreateCourse/CreateCourse';

import API from '../../store/services';
import { authorsSelector } from '../../store/authors/selectors';
import { coursesSelector } from '../../store/courses/selectors';
import { setAuthorsAction } from '../../store/authors/actionCreators';
import { setCoursesAction } from '../../store/courses/actionCreators';

import getAuthorsById from '../../helpers/getAuthorsById';
import pipeDuration from '../../helpers/pipeDuration';

const CourseContainer = () => {
	const courses = useSelector(coursesSelector);
	const authors = useSelector(authorsSelector);
	const dispatch = useDispatch();

	useEffect(() => {
		const coursesReq = API.getCoursesService();
		const authorsReq = API.getAuthorsService();
		Promise.all([coursesReq, authorsReq])
			.then(([coursesRes, authorsRes]) => {
				dispatch(setAuthorsAction(authorsRes));
				dispatch(setCoursesAction(coursesRes));
			})
			.catch((e) => {
				API.setError(e);
			});
	}, []);

	const coursesData = courses.map((course) => {
		const courseAuthors = getAuthorsById(authors, course.authors);
		const courseAuthorNames = courseAuthors.map((author) => author.name);
		return {
			...course,
			duration: pipeDuration(course.duration),
			authors: courseAuthorNames.join(', '),
		};
	});

	if (!coursesData.length) {
		return <div>Loading</div>;
	}

	return (
		<Routes>
			<Route
				index
				element={<Courses className='app__courses' coursesData={coursesData} />}
			/>
			<Route
				path='add'
				element={<CreateCourse getAuthorsById={getAuthorsById} />}
			/>
			<Route path=':id' element={<CourseInfo coursesData={coursesData} />} />
		</Routes>
	);
};

export default CourseContainer;
