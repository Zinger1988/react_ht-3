import React from 'react';
import PropTypes from 'prop-types';

import './Logo.scss';

const Logo = (props) => {
	const { className } = props;
	return <img className={`logo ${className}`} src='/logo.svg' alt='logo' />;
};

Logo.propTypes = {
	className: PropTypes.string,
};

Logo.defaultProps = {
	className: '',
};

export default Logo;
