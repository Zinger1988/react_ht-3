import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';

import { logoutUserAction } from '../../store/user/actionCreators';
import { useDispatch } from 'react-redux';

import './Header.scss';

const Header = (props) => {
	const { className, user } = props;
	const dispatch = useDispatch();

	const handleLogout = () => {
		localStorage.removeItem('token');
		localStorage.removeItem('user');
		dispatch(logoutUserAction());
	};

	return (
		<header className={`header ${className}`}>
			<div className='container header__container'>
				<Link className='header__logo' to='/'>
					<Logo className='header__logo-img' />
				</Link>

				{user.isAuth && (
					<>
						<div className='header__user'>{user.email}</div>
						<Button
							onClick={handleLogout}
							className='header__logout-btn'
							buttonText='Logout'
						/>
					</>
				)}
			</div>
		</header>
	);
};

Header.propTypes = {
	user: PropTypes.shape({
		name: PropTypes.string,
		email: PropTypes.string.isRequired,
	}),
	className: PropTypes.string,
};

Header.defaultProps = {
	className: '',
};

export default Header;
