import PropTypes from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import { Form, Formik } from 'formik';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import ShortMessage from '../../common/ShortMessage/ShortMessage';

import { errorsSelector } from '../../store/errors/selectors';
import { clearErrorsAction } from '../../store/errors/actionCreators';

import validationSchema from './validationSchema';
import API from '../../store/services';

import './Registration.scss';

const Registration = (props) => {
	const [isSubmitted, setIsSubmitted] = useState(false);
	const { isAuth, className } = props;
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const errors = useSelector(errorsSelector);

	const errorMessage = errors.map((error, i) => {
		const message = error.data.errors.join(', ');
		return <ShortMessage key={i} type='alert' message={message} />;
	});

	useEffect(() => {
		isAuth && navigate('/');
	}, [isAuth]);

	if (isSubmitted && !errors.length) {
		navigate('/login');
	}

	const handleSubmit = async (values) => {
		dispatch(clearErrorsAction());
		API.registerService(values).then(() => {
			setIsSubmitted(true);
		});
	};

	return (
		<div className={`registration ${className}`}>
			<div className='container container--narrow'>
				<h1 className='page-title'>Registration</h1>
				<Formik
					initialValues={{
						name: '',
						email: '',
						password: '',
					}}
					onSubmit={handleSubmit}
					validationSchema={validationSchema}
				>
					{() => (
						<Form className='registration__form form'>
							<div className='form__body'>
								<Input
									className='form__input'
									name='name'
									label='Name'
									placeholder='Enter your name...'
								/>
								<Input
									className='form__input'
									name='email'
									label='Email'
									placeholder='Enter your email...'
								/>
								<Input
									className='form__input'
									type='password'
									name='password'
									label='Password'
									placeholder='Enter your password...'
								/>
							</div>
							{errorMessage.length > 0 && (
								<div className='form__errors'>{errorMessage}</div>
							)}
							<div className='form__footer'>
								<Button
									type='submit'
									buttonText='Registration'
									cssStyle='solid-yellow'
									size='lg'
									fluid
								/>
							</div>
						</Form>
					)}
				</Formik>
				<p>
					If you have an account you can <Link to='/login'>login</Link>
				</p>
			</div>
		</div>
	);
};

Registration.propTypes = {
	className: PropTypes.string,
};

Registration.defaultProps = {
	className: '',
};

export default Registration;
