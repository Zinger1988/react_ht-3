import React from 'react';
import PropTypes from 'prop-types';

import './ShortMessage.scss';

const ShortMessage = (props) => {
	const { className, message, type } = props;
	return (
		<span className={`short-message short-message--${type} ${className}`}>
			{message}
		</span>
	);
};

ShortMessage.propTypes = {
	className: PropTypes.string,
	message: PropTypes.string,
	type: PropTypes.oneOf(['info', 'alert']),
};

ShortMessage.defaultProps = {
	className: '',
	type: 'info',
};

export default ShortMessage;
