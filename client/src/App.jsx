import React from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';

import CourseContainer from './components/CourseContainer/CourseContainer';
import Header from './components/Header/Header';
import Login from './components/Login/Login';
import Page404 from './components/Page404/Page404';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import Registration from './components/Registration/Registration';

import { useSelector } from 'react-redux';
import { userSelector } from './store/user/userSelector';

import './App.scss';

function App() {
	const user = useSelector(userSelector);

	return (
		<div className='app'>
			<Header className='app__header' user={user} />
			<main className='app__content'>
				<Routes>
					<Route path='/' element={<Navigate to='/courses' />} />
					<Route
						path='/courses/*'
						element={
							<ProtectedRoute isAuth={user.isAuth} redirectPath='/login'>
								<CourseContainer />
							</ProtectedRoute>
						}
					/>
					<Route
						path='/registration'
						element={<Registration className='my-auto' isAuth={user.isAuth} />}
					/>
					<Route
						path='/login'
						element={<Login className='my-auto' isAuth={user.isAuth} />}
					/>
					<Route path='*' element={<Page404 />} />
				</Routes>
			</main>
		</div>
	);
}

export default App;
