import { SET_AUTHORS, ADD_AUTHOR } from './actionTypes';
const initialState = [];

const authors = (state = initialState, action) => {
	switch (action.type) {
		case SET_AUTHORS:
			return [...state, ...action.payload];
		case ADD_AUTHOR:
			return [...state, action.payload];
		default:
			return state;
	}
};

export default authors;
