import { SET_AUTHORS, ADD_AUTHOR } from './actionTypes';

export const setAuthorsAction = (authors) => ({
	type: SET_AUTHORS,
	payload: authors,
});

export const addAuthorAction = (author) => ({
	type: ADD_AUTHOR,
	payload: author,
});
