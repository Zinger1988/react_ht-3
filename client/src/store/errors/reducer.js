import { SET_ERRORS, ADD_ERROR, CLEAR_ERRORS } from './actionTypes';
const initialState = [];

const errors = (state = initialState, action) => {
	switch (action.type) {
		case SET_ERRORS:
			return [...state, ...action.payload];
		case ADD_ERROR:
			return [...state, action.payload];
		case CLEAR_ERRORS:
			return [];
		default:
			return state;
	}
};

export default errors;
