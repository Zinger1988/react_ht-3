import { SET_ERRORS, ADD_ERROR, CLEAR_ERRORS } from './actionTypes';

export const setErrorsAction = (errors) => ({
	type: SET_ERRORS,
	payload: errors,
});

export const addErrorAction = (error) => ({
	type: ADD_ERROR,
	payload: error,
});

export const clearErrorsAction = () => ({
	type: CLEAR_ERRORS,
});
