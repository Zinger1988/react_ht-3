import { LOGIN_USER, LOGOUT_USER } from './actionTypes';

const initialState = {
	isAuth: false,
	name: '',
	email: '',
	token: '',
};

if (localStorage.getItem('token')) {
	initialState.isAuth = true;
	initialState.token = localStorage.getItem('token');
}

if (localStorage.getItem('user')) {
	const user = JSON.parse(localStorage.getItem('user'));
	initialState.name = user.name;
	initialState.email = user.email;
}

const user = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN_USER:
			const { token, ...userData } = action.payload;
			localStorage.setItem('token', token);
			localStorage.setItem('user', JSON.stringify(userData));
			return { isAuth: true, ...action.payload };
		case LOGOUT_USER:
			localStorage.removeItem('token');
			localStorage.removeItem('user');
			return {
				isAuth: false,
				name: '',
				email: '',
				token: '',
			};
		default:
			return state;
	}
};

export default user;
