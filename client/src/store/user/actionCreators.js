import { LOGIN_USER, LOGOUT_USER } from './actionTypes';

export const loginUserAction = (userData) => ({
	type: LOGIN_USER,
	payload: userData,
});

export const logoutUserAction = () => ({
	type: LOGOUT_USER,
});
