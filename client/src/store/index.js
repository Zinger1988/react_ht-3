import courses from './courses/reducer';
import authors from './authors/reducer';
import user from './user/reducer';
import errors from './errors/reducer';
import { createStore, combineReducers } from 'redux';

const rootReducer = combineReducers({
	authors,
	courses,
	user,
	errors,
});

const devtools =
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = createStore(rootReducer, devtools);

export default store;
