import { SET_COURSES, ADD_COURSE, DELETE_COURSE } from './actionTypes';
const initialState = [];

const courses = (state = initialState, action) => {
	switch (action.type) {
		case SET_COURSES:
			return [...state, ...action.payload];
		case ADD_COURSE:
			return [...state, action.payload];
		case DELETE_COURSE:
			return state.filter((course) => course.id !== action.payload);
		default:
			return state;
	}
};

export default courses;
