import { SET_COURSES, ADD_COURSE, DELETE_COURSE } from './actionTypes';

export const setCoursesAction = (courses) => ({
	type: SET_COURSES,
	payload: courses,
});

export const addCourseAction = (course) => ({
	type: ADD_COURSE,
	payload: course,
});

export const deleteCourseAction = (id) => ({
	type: DELETE_COURSE,
	payload: id,
});
