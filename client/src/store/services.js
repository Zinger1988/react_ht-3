import axios from 'axios';
import store from './index';
import { addErrorAction } from './errors/actionCreators';

const server = axios.create({
	baseURL: 'http://localhost:4000',
});

class API {
	constructor() {
		server.interceptors.request.use((req) => {
			console.log(req);
			return req;
		});
		server.interceptors.response.use((res) => {
			console.log(res);
			return res;
		});
	}

	loginService(userData) {
		return server
			.post('/login', userData)
			.then((res) => {
				return { token: res.data.result, ...res.data.user };
			})
			.catch((e) => {
				this.setError(e);
			});
	}

	registerService(userData) {
		return server.post('/register', userData).catch((e) => {
			this.setError(e);
		});
	}

	getCoursesService() {
		return server
			.get('/courses/all', this.getHeaderWithToken())
			.then((res) => res.data.result);
	}

	addCourseService(courseData) {
		return server
			.post('/courses/add', courseData, this.getHeaderWithToken())
			.then((res) => res.data.result);
	}

	deleteCourseService(id) {
		return server
			.delete(`/courses/${id}`, this.getHeaderWithToken())
			.then((res) => res.data.result);
	}

	getAuthorsService() {
		return server.get('authors/all').then((res) => res.data.result);
	}

	addAuthorService(authorData) {
		return server
			.post('authors/add', authorData, this.getHeaderWithToken())
			.then((res) => res.data.result);
	}

	getHeaderWithToken() {
		return { headers: { Authorization: store.getState().user.token } };
	}

	setError(e) {
		console.warn(e);
		this.dispatch(addErrorAction(e.response));
	}

	dispatch(action) {
		store.dispatch(action);
	}
}

export default new API();
